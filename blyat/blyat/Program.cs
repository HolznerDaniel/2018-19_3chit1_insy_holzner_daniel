﻿using ScottnewContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blyat
{
    class Program
    {
        static void Main(string[] args)
        {
            ScottnewDataContext sc = new ScottnewDataContext();

            //var e = from x in sc.Emps where x.DeptId >= 20 select x;
            //foreach (var item in e)
            //{
            //    Console.WriteLine(item.ENAME + " - " + item.Dept.DEPTNO + " - " + item.Dept.DNAME);
            //}

            Console.WriteLine("--------------------------------------------------NR1--------------------------------------------------");
            //NR 1
            var d = from x in sc.Depts select x;
            foreach (var item in d)
            {
                Console.WriteLine(item.DEPTNO + " - " + item.DNAME + " - " + item.LOC);
            }

            Console.WriteLine("--------------------------------------------------NR2--------------------------------------------------");
            //NR 2

            var e2 = from x2 in sc.Emps where x2.COMM > x2.SAL select x2;
            foreach (var item in e2)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR3--------------------------------------------------");
            //NR 3

            var e3 = from x3 in sc.Emps select x3;
            foreach (var item in e3)
            {
                Console.WriteLine(item.ENAME + " - " + item.JOB);
            }

            Console.WriteLine("--------------------------------------------------NR4--------------------------------------------------");
            //NR 4

            var e4 = from x4 in sc.Emps where x4.DeptId != 10 select x4;
            foreach (var item in e4)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR5--------------------------------------------------");
            //NR 5

            var e5 = from x5 in sc.Emps where x5.COMM > x5.SAL select x5;
            foreach (var item in e5)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR6--------------------------------------------------");
            //NR 6

            var e6 = from x6 in sc.Emps where x6.HIREDATE == new DateTime(1981, 12, 3) select x6;
            foreach (var item in e6)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR7--------------------------------------------------");
            //NR 7

            var e7 = from x7 in sc.Emps where x7.SAL < 1250 || x7.SAL > 1600 select x7;
            foreach (var item in e7)
            {
                Console.WriteLine(item.ENAME + " - " + item.SAL);
            }

            Console.WriteLine("--------------------------------------------------NR8--------------------------------------------------");
            //NR 8

            var e8 = from x8 in sc.Emps where x8.JOB != "MANAGER" &&  x8.JOB != "PRESIDENT" select x8;
            foreach (var item in e8)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR9--------------------------------------------------");
            //NR 9

            var e9 = from x9 in sc.Emps where x9.ENAME.Length >= 2 && x9.ENAME[2] == 'A' select x9;
            foreach (var item in e9)
            {
                Console.WriteLine(item.ENAME);
            }

            Console.WriteLine("--------------------------------------------------NR10--------------------------------------------------");
            //NR 10

            var e10 = from x10 in sc.Emps where x10.COMM != null select x10;
            foreach (var item in e10)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB);
            }

            Console.WriteLine("--------------------------------------------------NR11--------------------------------------------------");
            //NR 11

            var e11 = from x11 in sc.Emps where x11.COMM != null orderby x11.COMM select x11;
            foreach (var item in e11)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR12--------------------------------------------------");
            //NR 12

            var e12 = from x12 in sc.Emps where x12.JOB != "MANAGER" && x12.JOB != "PRESIDENT" orderby x12.HIREDATE select x12;
            foreach (var item in e12)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR13--------------------------------------------------");
            //NR 13

            var e13 = from x13 in sc.Emps where x13.ENAME.Length == 6 select x13;
            foreach (var item in e13)
            {
                Console.WriteLine(item.ENAME);
            }

            Console.WriteLine("--------------------------------------------------NR14--------------------------------------------------");
            //NR 14

            var e14 = from x14 in sc.Emps where x14.COMM > 0 select x14;
            foreach (var item in e14)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR15--------------------------------------------------");
            //NR 15

            var e15 = from x15 in sc.Emps where x15.COMM > 0 select x15;
            foreach (var item in e15)
            {
                Console.WriteLine(item.Id + " - " + item.ENAME + " - " + item.JOB + " - " + item.ParentId + " - " + item.HIREDATE + " - " + item.SAL + " - " + item.COMM + " - " + item.DeptId);
            }

            Console.WriteLine("--------------------------------------------------NR16--------------------------------------------------");

        }
    }
}
